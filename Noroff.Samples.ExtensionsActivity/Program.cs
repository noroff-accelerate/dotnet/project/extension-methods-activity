﻿namespace Noroff.Samples.ExtensionsActivity
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            string testSentence = "Hello, world!";
            int wordCount = testSentence.WordCount();
            Console.WriteLine($"Word count: {wordCount}");

            string palindromeTest = "A man, a plan, a canal: Panama";
            bool isPalindrome = palindromeTest.IsPalindrome();
            Console.WriteLine($"Is '{palindromeTest}' a palindrome? {isPalindrome}");

            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            int sumOfEvens = numbers.SumOfEvens();
            Console.WriteLine($"Sum of even numbers: {sumOfEvens}");
        }

    }
}