﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.ExtensionsActivity
{
    public static class StringExtensions
    {
        public static int WordCount(this string str)
        {
            return str.Split(new char[] { ' ', '.', '?' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }

        public static bool IsPalindrome(this string str)
        {
            string processedStr = new string(str.Where(char.IsLetterOrDigit).ToArray()).ToLower();
            return processedStr.SequenceEqual(processedStr.Reverse());
        }
    }

}
