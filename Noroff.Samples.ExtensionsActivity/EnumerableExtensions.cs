﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.ExtensionsActivity
{
    public static class EnumerableExtensions
    {
        public static int SumOfEvens(this IEnumerable<int> numbers)
        {
            return numbers.Where(n => n % 2 == 0).Sum();
        }
    }

}
