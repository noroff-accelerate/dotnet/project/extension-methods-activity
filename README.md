# Extension Methods Activity

## Overview
This activity is designed to teach you how to create and use extension methods in C#. Extension methods are a powerful feature in C# that allow you to add new methods to existing types without altering their code. This activity will guide you through the process of creating extension methods for the `String` class and `IEnumerable<int>` collection.

## Objectives
- Demonstrate the process of creating an extension method in C#, illustrating how to add new functionalities to existing classes without modifying them.
- Explain the significance and appropriate use cases for extension methods in C#, discussing their impact on code maintainability and readability.

## Tasks
1. Create a static class named `StringExtensions`.
2. Implement a method called `WordCount` in the `StringExtensions` class. This method should count the number of words in a string.
3. Test the `WordCount` method by applying it to a string in the `Main` method.
4. Implement an `IsPalindrome` method in the `StringExtensions` class to check if a string is a palindrome.
5. Test the `IsPalindrome` method with different string inputs in the `Main` method.
6. Reflect on how these extension methods allow adding functionality to the `String` class without modifying the original class.

### Bonus Challenge
- Create an extension method for `IEnumerable<int>` named `SumOfEvens` to calculate the sum of all even numbers.
- Test the `SumOfEvens` method with a list of integers in the `Main` method.

## Reflection
After completing the activity, reflect on the following points:

- How do extension methods adhere to the Open/Closed Principle?
- In what scenarios can extension methods improve code maintainability and readability?
- What are the limitations of extension methods?
